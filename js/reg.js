const accountValidator = new FieldValidator('txtLoginId', async (val) => {
    if (!val)
        return '账号不能为空'
    const resp = await exist(val)
    if (resp.data)
        return '账号已存在'
})

const nicknameValidator = new FieldValidator('txtNickname',  (val) => {
    if (!val)
        return '昵称不能为空'
})

const passwordValidator = new FieldValidator('txtLoginPwd', (val) => {
    if (!val)
        return '密码不能为空'
})

const passwordConfirmValidator = new FieldValidator('txtLoginPwdConfirm', (val) => {
    if (!val)
        return '确认密码不能为空'
    if (val !== passwordValidator.input.value)
        return '密码不一致'
})

const form = $('.user-form')
form.onsubmit = async (e) => {
    e.preventDefault()
    const result = await FieldValidator.validate(accountValidator,
        nicknameValidator,
        passwordValidator,
        passwordConfirmValidator)
    if (!result) return

    const formData = new FormData(form)
    const data = Object.fromEntries(formData)

    const resp = await reg(data)
    if (resp.code === 0) {
        alert('注册成功')
        location.href = 'http://localhost:63342/%E7%BD%91%E7%BB%9C/%E8%81%8A%E5%A4%A9%E6%9C%BA%E5%99%A8%E4%BA%BA/%E9%9D%99%E6%80%81%E9%A1%B5%E9%9D%A2/login.html'
    }
}