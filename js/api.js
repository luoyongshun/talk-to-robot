const BASE_URL = 'https://study.duyiedu.com'
const TOKEN_KEY = 'token'

const get = function (path) {
    const headers = {
        'Content-Type': 'application/json'
    }
    const token = localStorage.getItem(TOKEN_KEY)
    token && (headers.authorization = `Bearer ${token}`)
    return fetch(BASE_URL + path, {headers})
}

const post = function (path, bodyObj) {
    const headers = {
        'Content-Type': 'application/json'
    }
    const token = localStorage.getItem(TOKEN_KEY)
    token && (headers.authorization = `Bearer ${token}`)
    return fetch(BASE_URL + path, {headers, method: 'POST', body: JSON.stringify(bodyObj)})
}

/**
 * 注册账号
 * @param userInfo 用户信息
 */
const reg = async function (userInfo) {
    // 发送请求
    const resp = await post('/api/user/reg', userInfo)
    // 返回响应体
    return await resp.json()
}

/**
 * 登录
 * @param loginInfo 登录信息
 */
const login = async function (loginInfo) {
    // 发送请求
    const resp = await post( '/api/user/login', loginInfo)
    // 获取响应体
    const result = await resp.json()
    if (result.code === 0) { // 登录成功,将token保存到localStorage
        const token = resp.headers.get('Authorization')
        localStorage.setItem(TOKEN_KEY, token)
    }
    return result
}

/**
 * 账号是否存在
 * @param loginId
 */
const exist = async function (loginId) {
    // 发送请求
    const resp = await get(`/api/user/exists?loginId=${loginId}`)
    // 返回响应体
    return await resp.json()
}

/**
 * 账号信息
 */
const profile = async function () {
    // 发送请求
    const resp = await get('/api/user/profile')
    // 返回响应体
    return await resp.json()
}

/**
 * 发送聊天信息
 * @param content 聊天内容
 */
const sendChat = async function (content) {
    const resp = await post('/api/chat', {
        content
    })
    return await resp.json()
}

/**
 * 获取聊天记录
 */
const getHistory = async function () {
    const resp = await get('/api/chat/history')
    return await resp.json()
}

/**
 * 退出登录
 */
const logout = function () {
    localStorage.removeItem(TOKEN_KEY)
}