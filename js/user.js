/**
 * 为每个表单项添加一个验证器
 */
class FieldValidator {
    /**
     *
     * @param txtId 输入框id
     * @param validatorFunc 验证函数，传入输入框的值，返回错误消息，返回null表示验证成功
     */
    constructor(txtId, validatorFunc) {
        this.input = $('#' + txtId)
        this.p = this.input.nextElementSibling
        this.validatorFunc = validatorFunc
        // 输入框失去焦点
        this.input.onblur = async () => {
             await this.validate()
        }
    }

    /**
     * 验证，成功返回true,失败返回false
     * @returns {Promise<void>}
     */
    async validate() {
        const err = await this.validatorFunc(this.input.value)
        if (err) { // 有错误
            this.p.innerText = err
            return false
        } else {
            this.p.innerText = ''
            return true
        }
    }

    /**
     * 对传入的所有验证器进行统一验证，都成功返回true，否则返回false
     * @param validators
     * @returns {Promise<void>}
     */
    static async validate(...validators) {
        const proms = validators.map(v => v.validate())
        const result = await Promise.all(proms)
        return result.every(r => r)
    }
}