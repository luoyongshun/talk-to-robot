const accountValidator = new FieldValidator('txtLoginId', (val) => {
    if (!val)
        return '账号不能为空'
})

const passwordValidator = new FieldValidator('txtLoginPwd', val => {
    if (!val)
        return '密码不能为空'
})

const form = $('.user-form')
form.onsubmit = async (e) => {
    e.preventDefault()
    const result = await FieldValidator.validate(accountValidator, passwordValidator)
    if (!result) return
    const formData = new FormData(form)
    const data = Object.fromEntries(formData)
    const resp = await login(data)
    if (resp.code === 0) {
        alert('登录成功')
        // location.href = 'http://localhost:63342/%E7%BD%91%E7%BB%9C/%E8%81%8A%E5%A4%A9%E6%9C%BA%E5%99%A8%E4%BA%BA/%E9%9D%99%E6%80%81%E9%A1%B5%E9%9D%A2/index.html'
        location.href = 'file:///Users/luoyongshun/Desktop/%E5%89%8D%E7%AB%AF%E8%AF%BE%E7%A8%8B/%E7%BD%91%E7%BB%9C/%E8%81%8A%E5%A4%A9%E6%9C%BA%E5%99%A8%E4%BA%BA/%E9%9D%99%E6%80%81%E9%A1%B5%E9%9D%A2/index.html'
    } else
        alert('账号或密码错误')
}