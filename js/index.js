(async () => {
    //格式化日期
    function formatDate(timeStamp) {
        const date = new Date(timeStamp)
        const year = date.getFullYear()
        const month = (date.getMonth() + 1).toString().padStart(2, '0')
        const day = date.getDate().toString().padStart(2, '0')
        const hour = date.getHours().toString().padStart(2, '0')
        const minute = date.getMinutes().toString().padStart(2, '0')
        const second = date.getSeconds().toString().padStart(2,'0')
        return `${year}-${month}-${day} ${hour}:${minute}:${second}`
    }

    // 添加聊天记录
    const addChat = async (chatInfo) => {
        const div = $$$('div')
        div.classList.add('chat-item')
        if (chatInfo.from)
            div.classList.add('me')

        const img = $$$('img')
        img.classList.add('chat-avatar')
        chatInfo.from ? img.src = './asset/avatar.png' : img.src = './asset/robot-avatar.jpg'

        const chatContent = $$$('div')
        chatContent.classList.add('chat-content')
        chatContent.innerText = chatInfo.content

        const chatDate = $$$('div')
        chatDate.classList.add('chat-date')
        chatDate.innerText = chatInfo.createdAt
        // console.log(chatInfo.createdAt)

        div.appendChild(img)
        div.appendChild(chatContent)
        div.appendChild(chatDate)

        doms.chatContainer.appendChild(div)
    }

    // 设置用户信息
    const setUserInfo = () => {
        doms.aside.loginId.innerText = user.loginId
        doms.aside.nickname.innerText = user.nickname
    }

    // 滚动条滚动到底部
    const scrollBottom = () => {
        doms.chatContainer.scrollTop = doms.chatContainer.scrollHeight
    }

    //  发送聊天消息
    async function sendMessage() {
        //将聊天内容加到聊天框
        const content = doms.txtMsg.value
        addChat({
            content,
            createdAt: formatDate(Date.now()),
            from: user.loginId,
            to: null
        })
        doms.txtMsg.value = ''
        scrollBottom()
        //将聊天内容发送至服务器，返回机器人的聊天内容
        const resp = await sendChat(content)
        console.log(resp.data)
        addChat({
            content: resp.data.content,
            createdAt: formatDate(resp.data.createdAt),
            from: null,
            to: user.loginId
        })
        scrollBottom()
    }

    const resp = await profile()
    const user = resp.data
    if (!user) { // 登录失败
        alert(`${resp.msg}`)
        // location.href = 'http://localhost:63342/%E7%BD%91%E7%BB%9C/%E8%81%8A%E5%A4%A9%E6%9C%BA%E5%99%A8%E4%BA%BA/%E9%9D%99%E6%80%81%E9%A1%B5%E9%9D%A2/login.html'
        location.href = 'file:///Users/luoyongshun/Desktop/%E5%89%8D%E7%AB%AF%E8%AF%BE%E7%A8%8B/%E7%BD%91%E7%BB%9C/%E8%81%8A%E5%A4%A9%E6%9C%BA%E5%99%A8%E4%BA%BA/%E9%9D%99%E6%80%81%E9%A1%B5%E9%9D%A2/login.html'
        return
    }
    // 登录成功
    const doms = {
        aside: {
            nickname: $('#nickname'),
            loginId: $('#loginId')
        },
        close: $('.close'),
        chatContainer: $('.chat-container'),
        txtMsg: $('#txtMsg'),
        msgContainer: $('.msg-container')
    }

    setUserInfo()

    // 退出登录
    doms.close.onclick = function () {
        logout()
        // location.href = 'http://localhost:63342/%E7%BD%91%E7%BB%9C/%E8%81%8A%E5%A4%A9%E6%9C%BA%E5%99%A8%E4%BA%BA/%E9%9D%99%E6%80%81%E9%A1%B5%E9%9D%A2/login.html'
        location.href = 'file:///Users/luoyongshun/Desktop/%E5%89%8D%E7%AB%AF%E8%AF%BE%E7%A8%8B/%E7%BD%91%E7%BB%9C/%E8%81%8A%E5%A4%A9%E6%9C%BA%E5%99%A8%E4%BA%BA/%E9%9D%99%E6%80%81%E9%A1%B5%E9%9D%A2/login.html'
    }
    // 获取历史记录
    const resp1 = await getHistory()
    console.log(resp1.data)
    // 创建聊天对象
    for (const chat of resp1.data) {
        const  chatObj = {
            content: chat.content,
            createdAt: formatDate(chat.createdAt),
            from: chat.from,
            to: chat.to
        }
        await addChat(chatObj)
    }
    scrollBottom()

    doms.msgContainer.onsubmit = function (e) {
        e.preventDefault()
        sendMessage()
    }
})()
